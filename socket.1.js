#!/usr/bin/env node

"use strict";
const redis = require('redis');
const WebSocketClient = require('websocket').client;
let lastPing = new Date().getTime();
const markets = ["WETH-DAI", "GNO-WETH", "KNC-WETH"];
const client = redis.createClient();

//Connect to Redis
client.on('connect', function() {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

//Push data to redis
function saveData(response) {

    JSON.parse(response.utf8Data, ((key, value) => {
       if(typeof value === "string"){
        x(key,value);
       }
    }));

    function x (a, b){
        client.select(3, function(err,res){
        client.set(a, b, redis.print);
    client.get(a, function (error, result) {
    if (error) {
        console.log(error);
        throw error;
    }
    console.log('GET result ->'+ a + result);
});
});
    }
};



//Get data

function streamData(pair){
    const client = new WebSocketClient({tlsOptions: {rejectUnauthorized: false}});
    client.on('connectFailed', function(error) {
        console.log('Connect Error: ' + error.toString());
    });

    client.on('connect', function(connection) {
        console.log('Connected to Server...');
        connection.on('error', function(error) {
            console.log("Connection Error: " + error.toString());
        });
        connection.on('close', function() {
            console.log('Connection Closed');
        });
        connection.on('message', function(message) {
            if (message.type === 'utf8') {
                saveData(message);
            }
        });
        connection.on('pong', function(){
            console.log('[pingpong] response took', (new Date().getTime() - lastPing) + 'ms');
        })

        function send(message) {
            if (connection.connected) {
                connection.sendUTF(message);
            }
        }

        // subscribe with snapshot

        send(JSON.stringify(
            {
                "type": "SUBSCRIBE",
                "topic": "BOOK",
                "market": pair,
                "requestId": 1
              }
        ));

        // Send a ping every 10s
        // to keep the connection live
        setInterval(function(){
            lastPing = new Date().getTime();
            connection.ping();
        }, 10000);
    });

    client.connect('wss://ws.radarrelay.com/v2');
}


markets.forEach((market) => {
    streamData(market);
});
