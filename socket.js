#!/usr/bin/env node



"use strict";
// npm install websocket@1.0.25 --save
const WebSocketClient = require('websocket').client;
const client = new WebSocketClient({tlsOptions: {rejectUnauthorized: false}});
let lastPing = new Date().getTime();
var price;
var remainingQuoteTokenAmount;


client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    console.log('Connected to Server...');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('Connection Closed');
    });
    connection.on('message', function(message) {
       JSON.parse(message.utf8Data, function (key, value) {
            console.log(key + " "+value)
        });
    });
    connection.on('pong', function(){
      console.log('[pingpong] response took', (new Date().getTime() - lastPing) + 'ms');
      console.log(price + " / " + remainingQuoteTokenAmount)
    })

    function send(message) {
      if (connection.connected) {
          connection.sendUTF(message);
      }
    }
    
    // subscribe with snapshot
    send(`{
      "type": "SUBSCRIBE",
      "topic": "BOOK",
      "market": "WETH-DAI",
      "requestId": 1 
    }`);
  
  

    // Send a ping every 10s
    // to keep the connection live
    setInterval(function(){
      lastPing = new Date().getTime();
      connection.ping();
    }, 10000);
});

client.connect('wss://ws.radarrelay.com/v2');
