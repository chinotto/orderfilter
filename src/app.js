#!/usr/bin/env node
"use strict";

const api = require("./lib/streamData.js");
const markets = ["WETH-DAI", "USDC-DAI", "USDC-WETH"];

markets.forEach((market) => {
    api.streamData(market);
});
