#!/usr/bin/env node
"use strict";

/*
    This file gets data from API via websocket.
 */

const WebSocketClient = require('websocket').client;
const db = require("./saveData.js");
let lastPing = new Date().getTime();

function streamData(pair){
    const client = new WebSocketClient({tlsOptions: {rejectUnauthorized: false}});
    client.on('connectFailed', function(error) {
        console.log('Connect Error: ' + error.toString());
    });

    client.on('connect', function(connection) {
        console.log('Connected to Server...');
        connection.on('error', function(error) {
            console.log("Connection Error: " + error.toString());
        });
        connection.on('close', function() {
            console.log('Connection Closed');
        });
        connection.on('message', function(message) {
            if (message.type === 'utf8') {
                db.saveData(message, pair);
            }
        });
        connection.on('pong', function(){
            console.log('[pingpong] response took', (new Date().getTime() - lastPing) + 'ms');
        })

        function send(message) {
            if (connection.connected) {
                connection.sendUTF(message);
            }
        }

        // subscribe with snapshot

        send(JSON.stringify(
            {
                "type": "SUBSCRIBE",
                "topic": "BOOK",
                "market": pair,
                "requestId": 1
            }
        ));

        // Send a ping every 10s
        // to keep the connection live
        setInterval(function(){
            lastPing = new Date().getTime();
            connection.ping();
        }, 10000);
    });

    client.connect('wss://ws.radarrelay.com/v2');
};

module.exports.streamData = streamData;