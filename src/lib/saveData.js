#!/usr/bin/env node
"use strict";

/*
    This file saves data to redis.
 */


const redis = require('redis');
const client = redis.createClient();
let id = 0;
let db = 14;
let action;

//Connect to Redis
client.on('connect', function() {
    console.log('Redis client connected');
    });

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
    });

//Push data to redis
function saveData(response, pair) {
    console.log(response);
    switch (pair) {
        case "WETH-DAI":
            db = 0;
            break;
        case "GNO-WETH":
            db = 1;
            break;
        case "KNC-WETH":
            db = 2;
            break;
        };

    JSON.parse(response.utf8Data, ((key, value) => {
        if(key === "orderHash") {
            id = value;
            };
    }));

    JSON.parse(response.utf8Data, ((key, value) => {
        if(key === "action") {
            action = value;
        };
    }));

    if(action === "NEW" ){
        JSON.parse(response.utf8Data, ((key, value) => {
            if(typeof value === "string"){
                setData(key,value);
            }
        }));
    }  else { delData();};

};

function setData (a, b){
    client.select(db, function(err,res){
        client.hset(id, a, b);
    });

};

function delData (){
    client.select(db, function(err,res){
        client.del(id);
    });

};

module.exports.saveData = saveData;