#!/usr/bin/env node
"use strict";

let db = 0;
let priceDB0 = [];
let bidPrice = {bid: 1000000000000, vol: 0};
let askPrice = {bid: 0, vol: 0};


const redis = require('redis');
const client = redis.createClient();

function scan (callback){
    client.select(db, function(err,res){
        client.keys('*',(err, keys) => {
            if (err) return console.log(err);

            for(let i = 0, len = keys.length; i < len; i++) {
                priceDB0.push(keys[i]);
                client.hmget(keys[i], "type","price","remainingBaseTokenAmount", function(err, results) {
                    if(results[0] === "BID" && results[1] < bidPrice.bid){
                        bidPrice.bid = results[1];
                        bidPrice.vol = results [2];
                        console.log("bid = "+ bidPrice.bid +" bid Vol = "+ bidPrice.vol + " ask = " + askPrice.bid + " ask vol = " + askPrice.vol);
                    }
                    if(results[0] === "ASK" && results[1] > askPrice.bid){
                        askPrice.bid = results[1];
                        askPrice.vol = results [2];
                        console.log("bid = "+ bidPrice.bid +" bid Vol = "+ bidPrice.vol + " ask = " + askPrice.bid + " ask vol = " + askPrice.vol);
                    }

                });
            }
        });
        callback();
    });

};

function wait10sec(){
    setTimeout(function(){
        scan(wait10sec);
    }, 10000);
}

scan(wait10sec);
